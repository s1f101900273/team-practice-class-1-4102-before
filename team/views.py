from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.core.exceptions import SuspiciousOperation
from django.views.decorators.csrf import csrf_exempt
from typing import List

from .models import SearchHistory

from .search import search_photos
from .slack import check_correct_arg, generate_block_element

WEBHOOK_URL = (
    "https://hooks.slack.com/services/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"
)
VERIFICATION_TOKEN = "TC2jsdUcaECMqYg0dBrKZpPc"
ACTION_HOW_ARE_YOU = "HOW_ARE_YOU"
CLIENTID = "KGUEklw1v3csrXeDOrifcSyWyTgk7Yimt2JvPhFzDzU"


def index(request):
    histories = SearchHistory.objects.order_by("date")
    context = {"histories": histories}
    return render(request, "index.html", context)


@csrf_exempt
def search(request):
    if request.method == "POST":
        if not request.POST["keyword"]:
            return redirect(index)
        keyword = request.POST["keyword"]
        response = search_photos(keyword, CLIENTID)
        SearchHistory(
            user_id="guest",
            user_name="guest",
            keyword=keyword,
            total_hits=response.total,
        ).save()
    return redirect(index)


@csrf_exempt
def pic(request):
    if request.method != "POST":
        return JsonResponse({})

    if request.POST.get("token") != VERIFICATION_TOKEN:
        raise SuspiciousOperation("Invalid request.")

    user_name = request.POST["user_name"]
    user_id = request.POST["user_id"]
    contents: List[str] = request.POST["text"].split()
    if not check_correct_arg(contents):
        return JsonResponse(
            {
                "text": "Hi <@{}>! \n Usage: `/pic [keyword] [1 ~ 5]`".format(
                    user_id, user_name
                ),
                "response_type": "in_channel",
            }
        )
    keyword = contents[0]
    response = search_photos(contents[0], CLIENTID)
    display_count = 1 if len(contents) == 1 else int(contents[1])

    blocks = generate_block_element(response, display_count)
    result = {
        "blocks": blocks,
        "response_type": "in_channel",
    }
    SearchHistory(
        user_id=user_id, user_name=user_name, keyword=keyword, total_hits=response.total
    ).save()

    return JsonResponse(result)
