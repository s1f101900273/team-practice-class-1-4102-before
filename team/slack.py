from .search import SearchResult, Item
from typing import List


def check_correct_arg(contents: List[str]):
    return (
        (contents[1].isdigit() and int(contents[1]) > 0 and int(contents[1]) < 6)
        if len(contents) == 2
        else len(contents) == 1
    )


def generate_image_element(item: Item):
    return {
        "type": "image",
        "image_url": item.image_src,
        "alt_text": item.alt_desc,
        "title": {"type": "plain_text", "text": item.link},
    }


def generate_block_element(result: SearchResult, display_count: int):
    result_values = []
    for i in range(min(display_count, result.total)):
        result_values.append(generate_image_element(result.hits[i]))

    return (
        [
            {
                "type": "header",
                "text": {
                    "type": "plain_text",
                    "text": """Search results for "{}" """.format(result.keyword),
                },
            }
        ]
        + result_values
        + [
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": "Total: *{}* hits. searched for <https://unsplash.com/s/photos/{}|unsplash.com>".format(
                        result.total, result.keyword
                    ),
                },
            },
        ]
    )
