from team.search import Item, SearchResult
from django.test import TestCase
from team.slack import check_correct_arg, generate_block_element


class TestSearchModule(TestCase):
    def test_check_correct_arg(self):
        self.assertFalse(check_correct_arg(["test", "0"]))
        self.assertTrue(check_correct_arg(["test", "2"]))
        self.assertFalse(check_correct_arg(["test", "hoge"]))
        self.assertTrue(check_correct_arg(["test"]))
        self.assertFalse(check_correct_arg(["test", "1", "1"]))

    def test_generate_block_element(self):
        block = generate_block_element(
            SearchResult(
                2,
                "game",
                [
                    Item("https://example.com/photo", "https://example.com/1", "xxx"),
                    Item("https://example.com/icon", "https://example.com/2", "yyy"),
                ],
            ),
            1,
        )
        self.assertEqual(
            block,
            [
                {
                    "type": "header",
                    "text": {
                        "type": "plain_text",
                        "text": """Search results for "game" """,
                    },
                },
                {
                    "type": "image",
                    "image_url": "https://example.com/photo",
                    "alt_text": "xxx",
                    "title": {"type": "plain_text", "text": "https://example.com/1"},
                },
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": "Total: *2* hits. searched for <https://unsplash.com/s/photos/game|unsplash.com>",
                    },
                },
            ],
        )
