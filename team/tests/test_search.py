from django.test import TestCase
from ..search import generate_url, decode_response


class TestSearchModule(TestCase):
    def test_generate_url(self):
        base = "https://example.com"
        queryParam = {"token": "abcdefg", "id": "123456"}
        result = generate_url(base, queryParam)
        self.assertEqual(result, "https://example.com?token=abcdefg&id=123456")

    def test_decode_response(self):
        ## https://unsplash.com/documentation#response-16
        body = """
        {
          "total": 10,
          "results": [
            {
              "alt_description": "xxxx",
              "urls": {
                "regular": "https://example.com/photo"
              },
              "links": {
                "html": "https://example.com/index.html"
              }
            }      
          ]
        }
              """
        response = decode_response("game", body)
        self.assertEqual(response.total, 10)
        self.assertEqual(len(response.hits), 1)
        self.assertEqual(response.hits[0].alt_desc, "xxxx")
        self.assertEqual(response.hits[0].image_src, "https://example.com/photo")
        self.assertEqual(response.hits[0].link, "https://example.com/index.html")

