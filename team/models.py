from django.db import models


class SearchHistory(models.Model):
    user_name = models.CharField(max_length=100)
    user_id = models.CharField(max_length=100)
    keyword = models.CharField(max_length=100)
    total_hits = models.IntegerField(default=0)
    date = models.DateField(auto_now_add=True)
