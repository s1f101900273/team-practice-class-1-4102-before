import urllib.request
import urllib.parse
import json
from typing import List, Dict


class Item:
    def __init__(self, image, link, alt_desc):
        self.alt_desc: str = alt_desc if alt_desc is not None else ""
        self.image_src: str = image
        self.link: str = link


class SearchResult:
    def __init__(self, total: int, keyword: str, hits: List[Item]):
        self.total = total
        self.hits = hits
        self.keyword = keyword


def decode_response(keyword: str, json_string: str) -> SearchResult:
    payload = json.loads(json_string)
    total: int = payload["total"]
    hits: List[Item] = []
    for result in payload["results"]:
        hits.append(
            Item(
                alt_desc=result["alt_description"],
                image=result["urls"]["regular"],
                link=result["links"]["html"],
            )
        )
    return SearchResult(total, keyword, hits)


def generate_url(base: str, query: Dict[str, str]) -> str:
    return "{}?{}".format(base, urllib.parse.urlencode(query))


def request(req: urllib.request.Request) -> str:
    with urllib.request.urlopen(req) as res:
        return res.read().decode()


def search_photos(keyword: str, clientID: str):
    params = {
        "query": keyword,
        "client_id": clientID,
    }
    req = urllib.request.Request(
        generate_url("https://api.unsplash.com/search/photos", params)
    )
    body = request(req)

    return decode_response(keyword, body)


# response = searchPhoto('game')

# print(response.results[0].link)
